import event.EventDispatcher;
import event.listeners.ICallListener;

public class PhoneApplication implements ICallListener {
    public PhoneApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callstarted(int call_id) {
        System.out.println("udostepniono rozmowe dla uzytkownika");

    }

    @Override
    public void callended(int call_id) {
        System.out.println("uzytkownik zakonczyl polaczenie");
    }
}
