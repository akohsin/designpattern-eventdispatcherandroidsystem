import java.time.LocalDateTime;

public class PhoneCall {
private int call_id;
private LocalDateTime startTime;
private LocalDateTime endTime;
private boolean isOutgoing;

    public PhoneCall(int call_id, LocalDateTime startTime, boolean isOutgoing) {
        this.call_id = call_id;
        this.startTime = startTime;
        this.isOutgoing = isOutgoing;
        }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
