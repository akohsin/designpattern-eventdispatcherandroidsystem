import event.events.CallStartedEvent;
import event.EventDispatcher;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AndroidSystem system = new AndroidSystem();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String command = line.split(" ")[0];
            int call_id = Integer.parseInt(line.split(" ")[1]);
            if (command.equalsIgnoreCase("start")) {
                system.call(call_id);

            } else if (command.equalsIgnoreCase("stop")) {
               system.endCall(call_id);
            }
        }
    }
}
