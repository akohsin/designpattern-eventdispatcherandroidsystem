package event.events;

import event.EventDispatcher;
import event.listeners.ICallListener;
import event.IEvent;

public class CallEndedEvent implements IEvent {
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        for (ICallListener listener : EventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class)) {
            listener.callended(call_id);

        }
    }
}
