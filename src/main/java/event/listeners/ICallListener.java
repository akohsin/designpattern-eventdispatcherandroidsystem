package event.listeners;

public interface ICallListener {
    void callstarted(int call_id);
    void callended(int call_id);
}
