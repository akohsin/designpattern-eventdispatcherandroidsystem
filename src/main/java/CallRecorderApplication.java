import event.EventDispatcher;
import event.listeners.ICallListener;

import java.time.LocalDateTime;

public class CallRecorderApplication implements ICallListener {
    public CallRecorderApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callstarted(int call_id) {
        System.out.println(LocalDateTime.now().toString() + " rozpoczecie polaczenia id: " +call_id);

    }

    @Override
    public void callended(int call_id) {
        System.out.println(LocalDateTime.now().toString() + " zakonczenie polaczenia id: " +call_id);
    }
}
