import event.EventDispatcher;
import event.events.CallStartedEvent;
import event.listeners.ICallListener;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AndroidSystem implements ICallListener {
    private Map<Integer, PhoneCall> callRegistry = new HashMap<>();
    private PhoneApplication app = new PhoneApplication();
    private CallRecorderApplication rec = new CallRecorderApplication();
    private Integer actualCall = null;

    public AndroidSystem() {
        EventDispatcher.instance.registerObject(this);
    }

    public void call(int call_id) {
        if (actualCall == null) {
            EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
            callRegistry.put(call_id, new PhoneCall(call_id, LocalDateTime.now()));
        }
    }

    public void endCall(int call_id) {
        if (actualCall == call_id) {
            event.EventDispatcher.instance.dispatch(new event.events.CallEndedEvent(call_id));
            callRegistry.get(call_id).setEndTime(LocalDateTime.now());
            actualCall = null;
        }
    }

    @Override
    public void callstarted(int call_id) {
        System.out.println("Rozpoznano rozpoczęcie połączenia, callid: " + call_id);
        actualCall = call_id;
    }

    @Override
    public void callended(int call_id) {
        System.out.println("System zakonczyl polaczenie, callId: " + call_id);
    }
}
